
\captionsetup{justification=centering}
\chapter{Clusterização de dados}
\label{chapter:2}

  \section{Definição}

  Sistemas de Recomendação e Recuperação de Informação comumente usam
técnicas e metodologias de clusterização em atividades de Mineração de Dados
pra reconhecer e extrair padrões em um conjunto de dados.

Nesse tipo de análise, o objetivo é organizar
objetos em grupos ou clusters, de forma que os objetos do mesmo
cluster possuam uma semelhança significativa entre si por algum tipo de critério
estabelecido \cite{SIAM2007}. Imaginando então alguns
objetos sobre uma mesa, podemos querer agrupá-los por seu tamanho, cor, forma,
ou ainda por uma combinação de todas as três características. É possível realizar
essa separação apenas observando padrões sobre as qualidades dos objetos, não
sendo necessário nenhum treinamento anterior para identificar os diferentes
padrões. Na linguagem de aprendizado de máquina, essa técnica é descrita como
uma abordagem de classificação não supervisionada.

  O agrupamento de objetos é determinado em função de uma
medida de distância. Desta forma, o objetivo de um algoritmo de clusterização
é encontrar e classificar itens similares de maneira que a distância intra-cluster (soma das distância dos objetos
de um cluster até o seu centro) seja a menor
possível e a distância entre diferentes clusters seja maximizada, \cite{Ricci2011}.

  Esta abordagem se encaixa bem como uma alternativa para agrupar usuários
que votam de maneira parecida em comentários de uma conversa, pois consegue gerar
uma análise sobre os dados de modo a tratar individualmente cada conversa
com seu universo de características particular, como número de usuários, tema
da conversa, número de comentários e votos feitos por estes, o que não seria adequado
fazer com uma abordagem supervisionada.

  \section{Representação dos dados}

  Para que seja possível analisar um certo agrupamento de dados, é necessário
identificar quais informações podem ser utilizadas para
representar uma abstração prática dos objetos a serem agrupados. É preciso encontrar
então um conjunto de atributos que descrevem os itens para que
seja possível calcular o grau de semelhança entre os elementos.
Esses atributos, por sua vez, podem ser escritos de forma numérica, categórica,
binária e uma variedade de outros tipos. Como exemplo, podemos representar
pessoas em vetores compostos pelo valor numérico do ano de nascimento e um atributo
binário, 0 ou 1, para masculino ou feminino.

  \[
  \begin{bmatrix}
      pessoaA \\
      pessoaB \\
      pessoaC \\
      \hdotsfor{1} \\
      pessoaN
  \end{bmatrix}
  =
  \begin{bmatrix}
      1992 & 0 & \\
      2005 & 1 & \\
      1993 & 1 & \\
      \vdots & \vdots\\
      1985 & 0
  \end{bmatrix}
  \]

  Vejamos também, como a representação de um mesmo atributo, a cor de um objeto, pode
existir em diferentes formatos e escalas. 

  \begin{table}[!htbp]
    \begin{center}
      \caption{Representação nominal sobre a cor.}
      \begin{tabular}{|p{2cm}|p{2cm}|} \hline
        \textbf{Objeto} & \textbf{Cor} \\\hline
        A & Azul \\\hline
        B & Amarelo \\\hline
        C & Vermelho \\\hline
      \end{tabular}
    \label{table:objetosnominais}
    \end{center}
  \end{table}

  \begin{table}[!htbp]
    \begin{center}
      \caption{Representação categórica para o atributo cor.}
      \begin{tabular}{|p{2cm}|p{2cm}|p{2cm}|p{2cm}|}
        \hline
        \textbf{Objeto} & \textbf{Azul} & \textbf{Amarelo} & \textbf{Vermelho}\\\hline
        A & 1 & 0 & 0 \\\hline
        B & 0 & 1 & 0 \\\hline
        C & 0 & 0 & 1 \\\hline
      \end{tabular}
      \label{objetinho}
    \end{center}
  \end{table}

  \begin{table}[!htbp]
    \begin{center}
      \caption[Representação numérica para o atributo cor]{Representação numérica representada pela frequência da cor em Terahertz.}
      \begin{tabular}{|p{2cm}|p{2cm}|} \hline
        \textbf{Objeto} & \textbf{Cor (THz)} \\\hline
        A & 606\\\hline
        B & 508 \\\hline
        C & 400 \\\hline
      \end{tabular}
      \label{table:objetosnumericos}
    \end{center}
  \end{table}

  As Tabelas \ref{table:objetosnominais}, \ref{objetinho} e
\ref{table:objetosnumericos} mostram os mesmos dados representados de formas diferentes,
o formato depende basicamente da fonte dos dados, de como eles foram armazenados.
Para as diversas representações existe um tipo de função de distância compatível.
É possível, no entanto, utilizar qualquer uma das representações. \citeonline{SIAM2007}
detalham uma série de outros tipos e escalas de representação dos objetos.

  \section{Transformação dos dados}
  \label{data-transformation}

  Na maioria das aplicações do mundo real, o conjunto de dados precisa
  passar por algum tipo de tratamento antes de ser analisado \cite{Ricci2011}.
  Por exemplo, em casos onde a medida de similaridade
  é sensível a diferenças de magnitude e escalas das variáveis de entrada, como
  a distância Euclideana, faz-se necessário uma normalização de seus valores. Em
  outras situações onde os atributos dos objetos são de diferentes tipos, como
  categóricos e numéricos, há a necessidade de convertê-los para a mesma escala,
  ou buscar uma medida de similaridade que consiga tratar ambos os tipos de entradas.

  Além de simples conversões de escala e normalizações, pode ser adequado aplicar
  transformações mais complexas no conjunto de dados, como a redução de dimensionalidade.
  Para esta tarefa, existem várias técnicas, de complexidade linear e não linear,
  que podem gerar resultados diferentes de acordo com as características dos dados.
  No geral, uma técnica bastante utilizada para esta transformação e que também
  é usada neste trabalho é a Análise dos Componentes Principais (PCA).

  PCA é um método estatístico
  que permite representar em termos de outros eixos, com dimensões reduzidas.
  Estes eixos servem de base para uma projeção dos dados originais, ou seja,
  perde-se informação. Porém, essa projeção pode ainda representar suficientemente
  a quantidade de informação necessária para identificação de padrões e
  agrupamentos nos dados.

  Para encontrar os eixos ou componentes principais, calcula-se os autovetores
  e autovalores da matriz de covariância dos dados, resultando em um autovetor
  para cada variável existente. Os respectivos autovalores descrevem a variância
  relativa ao conjunto total. Pode-se então ignorar os componentes com menor
  contribuição para a variância para obter os dados projetados com dimensão
  reduzida. A Figura \ref{fig:pca-steps} é um diagrama dos passos
  da análise dos componentes principais. Mais detalhes sobre esta técnica podem
  ser vistos no Apêndice \ref{apendice:pca}.

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.5\linewidth]{figuras/pca.eps}
    \caption{Aplicação do PCA.}
    \label{fig:pca-steps}
  \end{figure}

  \section{Medidas de Similaridade}
  \label{sec:medidas-de-similaridade}

  Um ponto importante para análise de grupos com clusterização é definir uma
maneira para medir a similaridade entre os itens, pois para que seja possível
identificar elementos similares e agrupá-los em clusters, é preciso utilizar
alguma função que calcule, de forma quantitativa, o grau de
aproximação entre dois objetos distintos \cite{Toby2007}. A escolha da função de
distância é um passo importante, e geralmente se dá por uma
combinação de experiência, conhecimento e sorte, \cite{SIAM2007}.

Existem formas de cálculo que se adequam melhor a diferentes
tipos de dados, como cálculos específicos para atributos numéricos, categóricos,
binários, temporais, e também fórmulas que combinam atributos mistos. Uma
medida muito comum, usada em vários contextos e que também faz parte da nossa
proposta, é a distância Euclidiana simples, aplicável a dados numéricos. Esta medida é definida por:

  \[ \dis(x,y) = \sqrt{\sum_{k=1}^N (x_k - y_k)^2} \]

  Onde $N$ significa a quantidade de atributos e $x_k$ e $y_k$ são os k-ésimos
atributos que compõe os objetos $x$ e $k$ respectivamente.

  Se quisermos, por exemplo, calcular a similaridade entre entre sete pessoas considerando
as notas que deram em avaliações sobre dois filmes, podemos representá-las conforme
a Figura \ref{euclidean}. Os eixos representam as notas dos filmes em uma escala
de 0 a 5 e os pontos representam cada usuário. Pessoas que avaliam de maneira parecida
ficam mais próximos uns dos outras no gráfico.

  %FIGURA%
  \begin{figure}[!htb]
    \begin{center}
      \includegraphics[width=.65\linewidth]{figuras/filmes.eps}
      \caption{\label{euclidean}Representação de sete pessoas e suas avaliações.}
    \end{center}
  \end{figure}

  Para calcular a proximidade entre os usuários podemos utilizar
os valores das coordenadas de notas com na distância euclidiana e obter um número
absoluto, e quanto mais próximo de 0 esse valor, mais similares são as
pessoas.

  Um dos problemas com esta medida é que atributos com uma
escala maior que outros podem influenciar negativamente no resultado final, distorcendo-a.
Por exemplo, se a nota de uma pessoa é em uma escala de 0 a 50 e a de outra
é dada de 0 a 5, elas terão um peso diferente no cálculo da semelhança, já que
seus resultados se baseiam nos quadrados da raiz. Em cenários com variáveis em
escalas diferentes onde, por exemplo, uma delas mede o tempo e a outra mede a distância,
é necessário fazer uma normalização dos atributos para minimizar a distorção, porém
perde-se a informação de escala.

Outras medidas para dados numéricos e outros
tipos de dados são apresentadas no Apêndice \ref{apendice:medidas}. 

  \section{Modelos de Análise}

  A noção de cluster é algo que não possui uma definição precisa, e por isso há uma
grande variedade de algoritmos e abordagens para modelar problemas de agrupamento
de dados. Diferentes pesquisadores aplicam diferentes modelos de clusters, e para
cada modelo existem diversos algoritmos. As propriedades que definem o que é
um cluster variam significativamente de um para outro, bem como complexidade e
aplicação dos mesmos. Entender efetivamente estes modelos é a chave para entender os diferentes
algoritmos. \citeonline{SIAM2007} sumariza os conceitos de várias abordagens
existentes, como modelos hierárquicos, modelos baseados em grafos, densidade e outros.

  \begin{figure}[!h]
    \centering
    \includegraphics[width=1\linewidth]{figuras/overview-algorithms.eps}
    \caption[Vários modelos de algoritmos aplicados nos mesmos dados.]{Vários modelos de algoritmos aplicados nos mesmos dados.
    \newline Fonte: \citeonline{sklearn2016}}
    \label{fig:cluster-models}
  \end{figure}

  Para se ter uma ideia da variedade de resultados possíveis, vejamos a Figura
\ref{fig:cluster-models} que exibe a aplicação de diferentes algoritmos
sobre os mesmos conjuntos de dados. Estes algoritmos são implementados
na biblioteca scikit-learn\footnote{\url{http://scikit-learn.org/stable/modules/clustering.html}},
escrita em Python, e são apenas uma parte do universo total de opções.
Podemos ver em cada uma das quatro linhas um conjuntos de dados diferente e
em cada coluna temos o resultado da aplicação de um algoritmo naquele
conjunto. Os modelos e algoritmos aplicados são os baseados em centroide
(MiniBatchKmeans, MeanShift), algoritmos baseados em grafo (Spectral), alguns da classe
de modelos hierárquicos (Ward, AglomerativeClustering), e também um que faz análise
de densidade (DBSCAN).

  A escolha do modelo geralmente parte da observação e experiências de aplicação
em problemas semelhantes, já que conhecer e testar diferentes abordagens e suas
variações não é uma tarefa simples. Então, neste trabalho nos limitaremos ao estudo
e características do modelo de algoritmos baseados em centroide.

\subsection{Modelo centroide}

  Algoritmos baseados em centroides são muito eficientes em clusterização de
grandes bases de dados e com alta dimensionalidade quando comparados a outros modelos
\cite{SIAM2007}, embora não sejam muito adequados para encontrar clusters com
formatos arbitrários. Esse tipo de algoritmo representa cada cluster através das
coordenadas de seu centro e se guia por uma função objetivo que define o quão boa
é uma solução calculada e, a cada iteração, procuram agrupar membros aos clusters
de forma que a função seja minimizada. Um clássico algoritmo desse modelo é o $k$-means.

  O $k$-means é um algoritmo de clusterização bastante utilizado, rápido e de fácil implementação, \cite{SIAM2007}.
Ele requer o fornecimento de um parâmetro $k$ que determina o número de clusters desejado,
e uma função de erro é associada à sua execução. Desta forma, dado um $k$ inicial,
são operadas várias iterações que alocam repetidamente os dados ao conjunto mais
próximo, até que não haja alterações significativas na função de erro ou nos membros
de cada cluster. A descrição do algoritmo é vista a seguir.

  Seja $D$ um conjunto de dados qualquer, e seja $C_{1}$, $C_{2}$, $...$, $C_{k}$
os $k$ clusters disjuntos de $D$, então a função de erro é definida por:

  \[ E = \sum_{i=1}^K \sum_{k=1}^N d(X_{k},\mu(C_{i}))\]

  Onde o resultado é a soma das distâncias entre cada elemento X e o centro de
seu cluster. 

  O algoritmo é dado em duas fases, inicialização e iteração. Na fase de inicialização
são selecionados $k$ pontos aleatórios como centroides iniciais e os pontos
são distribuídos randomicamente nos $k$ grupos. Na segunda fase, de iteração,
associa-se cada ponto ao cluster de centro mais próximo, e ao final, os centros
de cada cluster são atualizados em função dos seus membros. A fase de iteração
é repetida até que não haja mudança nos clusters ou até que seja atingido um
limite do número de iterações pré-definido. Esse algoritmo pode então ser utilizado
para calcular $k$ grupos de opinião em uma conversa.
\newline

  \lstset{language=HTML}
  \begin{lstlisting}
    -> Inicialização
    Escolha k pontos, dentro do conjunto de pontos como centroides iniciais
    Associe cada ponto a um cluster aleatório
    -> Iteração
    Escolha k pontos, dentro do conjunto de pontos como centroides iniciais
    Repita:
      Forme k clusters movendo os pontos para o cluster de centro mais próximo
      Recalcule o centro de cada cluster baseado nos pontos pertencentes ao mesmo
    Até que os centroides não mudem.
  \end{lstlisting}

  A complexidade computacional do algoritmo é $O(nkd)$ por iteração,
onde $d$ é a dimensão dos dados, $k$ é o número de clusters e $n$ é o número de
pontos no conjunto de dados e sua solução converge para uma ótima local e não global,
dado que o problema resolvido pelo algoritmo é de natureza NP-difícil e seria
inviável testar todas as soluções possíveis para então optar pela melhor.
Desta forma, a qualidade da solução é bastante dependente dos $k$ centros escolhidos
na fase de inicialização.

  \section{Validação}
  Vários algoritmos são propostos na literatura de clusterização para diferentes
aplicações e diferentes tamanhos de dados. Por ser um processo não supervisionado,
não existem classes pré-definidas e nem exemplos que possam mostrar que os clusters
encontrados pelo algoritmo são válidos, \cite{halkidi2002}. E ainda, validar
o número ótimo de clusters é um grande desafio quando este parâmetro não é dado
pelo próprio algoritmo.

De fato, não existe um conceito de análise que seja
absoluto para avaliação de algoritmos de clusterização, mas existem uma série de
medidas que vêm de diversas áreas, como estatística e visão computacional, que
podem ser aplicadas \cite{imWalde2006}. Duas principais perspectivas devem ser
levadas em conta para avaliação do modelo, avaliação interna e externa, ambas
são aplicadas nesse trabalho.

  \subsection{Validação interna}

  No contexto de clusterização de maneira geral, quando não existem dados já
  classificados, de modo manual ou automático,
  que possam ser utilizados como referência para a saída esperada do algoritmo,
  aplicam-se métricas de avaliação interna. Nesses casos, o tipo de avaliação feito usa medidas
  que testam o modelo sobre ele próprio, o qual é executado com diferentes
  parâmetros e uma análise sobre seus resultados é aplicada com um
  algum índice que avalia, por exemplo, a densidade dos clusters
  gerados, ou a distância mínima entre os clusters, etc.

  O coeficiente de silhueta se encaixa nesse contexto, e é bastante utilizado como
  medida de qualidade para
  auxiliar na seleção do melhor número de clusters $k$ em algoritmos que
  requerem este parâmetro, como $k$-means. Essa métrica calcula um índice de
  avaliação para cada elemento do conjunto de dados. Para obter o coeficiente de
  silhueta \textit{sil} de um determinado objeto
  $i$ pertencente a um cluster $C_{A}$, é comparada a distância média $a$ entre
  o objeto $i$ e os outros objetos contidos no mesmo cluster $C_{A}$
  com a distância média $b$ entre $i$ e todos os objetos de um cluster vizinho
  mais próximo $C_{B}$. Assim, de acordo com a equação, aplica-se $-1 < sil(i) < 1$
  para cada objeto. Se $sil(i)$ é um valor alto, então a média das distâncias
  dentro do cluster é menor que a média de distâncias até o cluster vizinho, então
  os objeto $i$ foi bem classificado, caso contrário, ele foi mal classificado.

      \[a(i) = \frac{1}{|C_{A}| - 1} \sum_{j \in C_{A}, i \neq j} d(i, j) \]

      \[b(i) = min_{C_{B} \neq C_{A}} \frac{1}{|C_{B}|} \sum_{j \in C_{B}} d(i, j) \]

      \[sil(i) = \frac{b(i) - a(i)}{max\{a(i), b(i)\}} \]

  O coeficiente individual de cada objeto pode ainda ser expandido para um coeficiente
  para cada cluster ou ainda um coeficiente geral de todo o conjunto aplicado a
  um número $k$ de clusters. Para isso, basta tirar as médias para a amostragem desejada,
  por cluster ou por todo o conjunto:

      \[sil(C_{p}) = \frac{1}{C_{p}} \sum_{j \in C_{p}} sil(j) \]

      \[sil(k) = \frac{1}{k} \sum_{p=1}^k sil(C_{p}) \]

  Outra medida bastante conhecida que pode ser utilizada para avaliação interna
  é a Soma do Erro Quadrado. Com ela, busca-se quais parâmetros do algoritmo
  minimizam esta função, o que indicaria clusters mais homogêneos.
  A soma dos erros quadrados $E$ refere-se primariamente ao uso da distância
  Euclideana, mas é aplicável também a outras medidas, e é dada pela soma das distâncias
  de cada ponto até o centro de seu cluster:

    \[E(C) = \sum_{i=1}^k \sum_{O \in C_{i}} d(O_{i}, cen_{i})^2 \]


  \subsection{Validação externa}

  Quando possuímos valores de referência externos, que podem ser utilizados
  como resultado esperado, podemos então avaliar o modelo aplicando métricas similares
  às utilizadas na categoria de algoritmos supervisionados para avaliar uma taxa
  de acerto do algoritmo.

    \label{item:indice-de-rand}
    O Índice de Rand é uma medida para
    calcular a acurácia da classificação feita pelo algoritmo em relação a uma
    saída esperada esperada. \citeonline{Rand1971} define esta métrica de avaliação
    para problemas gerais de clusterização com base em uma análise sobre a correspondência
    entre os pares de objetos classificados. Dadas duas partições diferentes
    C e M feitas sobre um mesmo conjunto de dados:

    \begin{table}[!htb]
      \begin{center}
      \caption{Partição esperada $\times$ gerada para um cenário hipotético.}
      \label{my-label}
    \resizebox{.4\textwidth}{!}{%
      \begin{tabular}{|l|l|}
        \hline
        \multicolumn{1}{|c|}{\begin{tabular}[c]{@{}c@{}}Classificação\\ Esperada (M)\end{tabular}} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}Classificação do\\ Algoritmo (C)\end{tabular}} \\ \hline
          \begin{tabular}[c]{@{}l@{}}M1=\{a,b,c\}\\ M2=\{d,e,f\}\end{tabular}                        & \begin{tabular}[c]{@{}l@{}}C1 = \{a,b\}\\ C2 = \{c,d,e\}\\ C3 = \{f\}\end{tabular}         \\ \hline
      \end{tabular}}
      \end{center}
    \end{table}

    \noindent
    Sejam, A, B, C e D dados como:

    \begin{itemize}
     \item[*]{A $\rightarrow$ Número de pares de objetos no mesmo grupo tanto em M quanto em C,
     ou seja, $[(a,b), (d,e)] = 2$}

     \item[*]{B $\rightarrow$ Número de pares de objetos em grupos separados
     tanto em M quanto em C, ou seja, $[(a,d), (a,e), (a,f), (b,d),(b,e),(b,f),(c,f)] = 7$}

     \item[*]{C $\rightarrow$ Número de pares de objetos que estão no mesmo grupo em M, mas em
     diferentes grupos em C, ou seja, $[(a,c), (b,c), (d,f), (e,f)] = 4$}

     \item[*]{D $\rightarrow$ Número de pares de objetos que estão em grupos diferentes em M,
     mas no mesmo grupo em C, ou seja, $[(c,d), (c,e)] = 2$}
    \end{itemize}

    Então, pela definição do índice de Rand, a acurácia da partição C gerada pelo
    algoritmo em relação à partição esperada M é:
    
    \[ Rand(C, M) = \frac{A+B}{A+B+C+D} = \frac{A+B}{{{n}\choose{2}}} = \frac{2+7}{2+7+4+3} = 0.6 \]

    \citeonline{hubert1985} definem ainda um ajuste para a medida do índice de Rand, de
    forma que seu limite de valores fique entre $-1 \leq indice \leq 1$, mostrando que,
    em resultados negativos, siginifica que as partições comparadas apresentam
    comportamento aleatório.

    Estas são apenas algumas das várias medidas existentes que podem auxiliar na avaliação
    de modelos e algoritmos de clusterização. Para escolha da medida adequada
    apresentamos na próxima Seção, um estudo comparativo feito por \citeonline{imWalde2006}
    sobre métricas de validação.

  \subsection{Comparação entre medidas de validação}
  \label{sec:comparacaoValidacao}
    
    Na atividade de validação, \citeonline{imWalde2006} define
    quatro demandas que guiam um estudo comparativo entre métricas de qualidade
    de clusters.

    \begin{enumerate}
    \item Dado que o propósito da avaliação é comparar diferentes experimentos
    de clusterização e resultados, a métrica deve ser aplicável a todas as distâncias
    de similaridade usadas em clusterização, e ser também independente e não
    enviesada com a respectiva medida.

    \item A métrica deve definir uma medida numérica indicando o valor que deve
    ser de fácil interpretação ou ilustração quanto à sua extensão e efeitos, para
    facilitar a avaliação.

    \item A métrica de avaliação deve ser intransigente quanto à especificação
    do número e tamanho dos clusters.

    \item A métrica de avaliação deve ser capaz de distinguir a qualidade individual
    de cada cluster e a qualidade total de todo o particionamento.
    \end{enumerate}

    Com essas demandas, \citeonline{imWalde2006} monta também uma tabela comparativa
    entre algumas métricas com os seguintes levantamentos:

    \begin{itemize}
      \item[$\rhd$] Precisa de um valor de referência?

      \item[$\rhd$] É aplicável a todas as medidas de similaridade?

      \item[$\rhd$] É independente da medida de similaridade?

      \item[$\rhd$] Distingue valores para cada cluster e geral pra toda a partição?

      \item[$\rhd$] É enviesada ao número de clusters?

      \item[$\rhd$] É sensível à adição de erros (monotônica)?

      \item[$\rhd$] Qual é o intervalo de valores?
    \end{itemize}

\begin{table}[htb!]
  \centering
  \label{table:comparacaoDeMetricasDeValidacao}
  \caption[Comparação de métricas de avaliação]{Comparação de métricas de avaliação, \cite{imWalde2006} adaptado.}
  \resizebox{.9\textwidth}{!}{%
  \begin{tabular}{|l|c|c|c|c|c|}
    \hline
    \multirow{2}{*}{Métrica} & \multirow{2}{*}{Referência} & \multicolumn{2}{c|}{Medida de Similaridade} & \multicolumn{2}{c|}{Valor} \\ \cline{3-6} 
                                                              &                             & Aplicável           & Independente          & Específico     & Geral     \\ \hline
                                                              Erro Quadrado                                                 & não                           & sim                   & não                     & sim              & sim         \\ \hline
                                                              Silhueta                                                      & não                           & sim                   & não                     & sim              & sim         \\ \hline
                                                              Rand Index                                                    & sim                           & sim                   & sim                     & não              & sim         \\ \hline
                                                              Rand Index$_{ajustado}$                                       & sim                           & sim                   & sim                     & não              & sim         \\ \hline
                                                                Informação Mútua                                            & sim                           & sim                   & sim                     & sim              & sim         \\ \hline
                                                              Fowlkes-Mallow                                                & sim                           & sim                   & sim                     & não              & sim          \\ \hline
  \end{tabular}}
\vspace*{.5cm}

\resizebox{.9\textwidth}{!}{%
\begin{tabular}{|l|c|c|c|c|}
\hline
\multirow{2}{*}{Métrica}                                             & \multirow{2}{*}{Bias} & \multirow{2}{*}{Sensivel a Erros(monotônica)} & \multicolumn{2}{c|}{Interpretação} \\ \cline{4-5} 
                                                                &                       &                                   & Mín        & Máx       \\ \hline
  Erro Quadrado                                                 & muito pequeno         & não                                 & 0                & $\infty$               \\ \hline
  Silhueta                                                      & muito pequeno         & não                                 & -1               & 1               \\ \hline
  Rand Index                                                    & muito pequeno         & sim                                 & 0                & 1               \\ \hline
  Rand Index$_{ajustado}$                                       & não afetado           & sim                                 & -1                & 1               \\ \hline
  Informação Mútua                                              & muito pequeno         & sim                                 & 0                & $\infty$              \\ \hline
  Fowlkes-Mallow                                                & não afetado           & sim                                 & 0                & 1               \\ \hline
\end{tabular}}
\label{table:evaluation-measures}
\end{table}


A principal distinção entre cada medida é se ela necessita ou não de valores de
referência. As medidas do Error Quadrado e Silhueta, não precisam destes dados,
elas medem a qualidade dos clusters em referência aos próprios dados clusterizados,
e são muito utilizadas para auxiliar para definição do melhor número de clusters $K$
em algoritmos que requerem esse parâmetro.

A Tabela \ref{table:evaluation-measures} mostra os pontos fortes e fracos individuais
de cada métrica. Uma escolha mais adequada é uma medida que dê a qualidade de
cada cluster e um valor geral para toda a partição, optando por métricas
que possuam um intervalo de valores definido, o que facilita a interpretação. 
É importante também perceber se esta é afetada pelo tamanho dos clusters, causando
um \textit{bias}, e se é capaz de detectar a introdução de erros de maneira monotônica.

Como estamos desenvolvendo uma estratégia de clusterização baseada nos algoritmos
do modelo centroide, que requer o número $k$ de clusters, precisamos de uma medida que nos auxilie
para definição do melhor valor de $k$, como a Silhueta. E para compararmos
os resultados obtidos com valores de referência gerados manualmente, o Índice de
Rand ajustado e a medida de Fowlkes-Mallow são boas opções que podem ser aplicados.
Essas medidas fornecem insumos que irão nos auxiliar na construção de um algoritmo
que tenha uma maior coerência nos seus resultados.

