\chapter{Implementação da Solução}
\label{chapter:6}

  Este capítulo descreve a etapa prática de desenvolvimento do software
  que contempla a solução proposta e resultados obtidos. São apresentados aspectos técnicos e decisões
  tomadas para se atingir o objetivo do trabalho de desenvolver um modelo de
  algoritmo capaz de identificar grupos de opinião em um debate online. Detalhamos
  a seguir como foi desenvolvida a aplicação que é capaz de gerenciar conversas,
  comentários e votos de usuários e identificar grupos de opinião avaliando
  usuários que votam de maneira parecida. Vale ressaltar que neste trabalho focamos
  no desenvolvimento da lógica do modelo e não foram desenvolvidos nenhum meio de
  interação externa com o sistema ou interface gráfica.

\section{Sistema Django}

  Como já citado, o Django é um \textit{framework} para a criação de sistemas web,
  o qual utilizamos para o desenvolvimento da aplicação. Foram
  criados dois módulos: o módulo \textbf{Conversations} e o módulo
  \textbf{Clusters}.

  O módulo Conversations contém a lógica e estrutura que trata das conversas,
  votos e comentários. Já o módulo Clusters possui os algoritmos de clusterização,
  responsável por fazer o processamento dos dados das conversas. Consideramos então
  as seguintes relações para o desenvolvimento do sistema:

  \begin{enumerate}
    \item Um usuário pode criar várias conversas;
    \item Um usuário pode criar vários comentários em uma conversa;
    \item Um usuário pode votar em vários comentários de uma conversa;
    \item Um usuário que cria um comentário ou um voto em uma conversa se torna um participante desta;
    \item Um usuário que cria um comentário em uma conversa automaticamente recebe um voto de
    concordância com o mesmo;
    \item Um usuário pode participar de várias conversas;
    \item Uma conversa possui vários usuários participantes;
    \item Uma conversa possui uma clusterização associada;
  \end{enumerate}

  Com essas relações em mente, construimos diagrama de entidades e relacionamentos
  que mapeia conceitualmente no banco de dados, a ligação entre os objetos da
  aplicação. A Figura \ref{fig:der-diagram} mostra esse diagrama.
  \begin{figure}[!htb]
    \centering
    \includegraphics[width=0.9\linewidth, center]{figuras/der-diagram.eps}
    \caption{Diagrama de Entidades e Relacionamentos - DER.}
    \label{fig:der-diagram}
  \end{figure}

  As entidades Conversation, Vote, Comment e Participant, fazem parte do módulo
  Conversations. A entidade ClusterData faz parte do módulo Clusters, que é
  o componente que contempla toda a lógica do modelo de clusterização. O módulo
  Clusters possui também outras duas classes, que não são persistidas no banco
  de dados e por isso não aparecem no diagrama da Figura \ref{fig:der-diagram} acima.
  Essas classes compartilham da responsabilidade de manipular os dados para o cálculo
  dos clusters e merecem ser apresentadas:

  \begin{itemize}
    \item{\textbf{ClusterManager: }} responsável por iniciar a
    computação dos clusters de uma conversa a partir de um novo voto em um comentário.
    A partir dela são recuperados os dados de uma conversa, aplicados os
    algoritmos e persistidos os resultados.

    \item{\textbf{ConversationDataMatrix: }} oferece recursos para recuperação e
    preparação do conjunto de dados de uma conversa (participantes $\times$ comentários),
    oferecendo também as transformações necessárias aos dados, como a redução de
    dimensionalidade através do PCA.

    \item{\textbf{ClusterData: }} esta classe guarda abstração dos resultados de
    uma clusterização. Ela armazena informações do cálculo como, número de clusters,
    coordenadas dos centros de cada clusters, membros de cada cluster e coeficiente
    de silhueta. Essa classe tem suas informações persistidas no banco de dados.
  \end{itemize}

  Cada módulo foi concebido segundo a arquitetura de \textit{apps} do framework
  Django. A estrutura dos \textit{apps} é representada conforme a Figura \ref{fig:arquitetura-apps},
  as classes de cada \textit{app} em retângulos brancos e funções em vermelho.
  Estabelecemos assim, uma comunicação entre os dois apps através de um sinal que
  é enviado para a classe ClusterManager cada vez que um novo voto é inserido
  no sistema, agindo como um ativador para cálculo dos clusters.

  \begin{figure}[!htb]
    \centering
    \includegraphics[scale=.55, center]{figuras/pentano-architecture.eps}
    \caption{Estrutura dos \textit{apps} Conversations e Clusters.}
    \label{fig:arquitetura-apps}
  \end{figure}

\section{Validação do modelo}

  Para a validação dos resultados foi tomada a decisão de produzir manualmente
  usuários, comentários e votos cujos valores serão utilizados como referência. Estes dados
  são submetidos ao algoritmo e uma análise da taxa de acerto é feita segundo o
  índice de Rand ajustado, apresentado na Seção \ref{sec:comparacaoValidacao}

  \subsection{Definição dos casos de teste}

  Para uma análise mais abrangente, definimos diferentes cenários para a validação,
  variando a quantidade de grupos esperados, quantidade de participantes e comentários na
  conversa. Definimos em cada cenário a quantidade de grupos e suas características, de modo
  a manipular os votos para gerar grupos da maneira desejada, possibilitando, assim,
  uma comparação entre a classificação esperada e a obtida pelo algoritmo.
  A Tabela \ref{table:cenarios-de-teste} mostra os cenários considerados nos testes.
  Buscamos cobrir diferentes intervalos de teste para obter uma melhor análise do
  modelo.

  \begin{table}[!htbp]
    \centering
    \caption{Cenários de teste do algoritmo.}
    \begin{tabular}{|c | c | c| c| c| c| c| c| c|} \hline
      \rowcolor{lightgray} Cenário &  N grupos/clusters &  N comentários  &  N membros/grupo &  Total participantes \\\hline
      1       &  2        &  8              &  4               &  8        \\\hline
      2       &  2        &  50             &  5               &  10       \\\hline
      3       &  2        &  50             &  10              &  20       \\\hline
      4       &  2        &  150            &  10              &  20       \\\hline
      5       &  3        &  8              &  4               &  8       \\\hline
      6       &  3        &  50             &  5               &  15       \\\hline
      7       &  3        &  50             &  10              &  30       \\\hline
      8       &  3        &  150            &  10              &  30       \\\hline
      9       &  4        &  50             &  5               &  20       \\\hline
      10      &  4        &  50             &  10              &  40       \\\hline
      11      &  4        &  150            &  10              &  40       \\\hline
      12      &  5        &  50             &  5               &  25       \\\hline
      13      &  5        &  50             &  10              &  50       \\\hline
      14      &  5        &  150            &  10              &  50       \\\hline
    \end{tabular}
    \label{table:cenarios-de-teste}
  \end{table}

  \subsection{Obtenção dos votos dos usuários}

  Para gerar os votos em cada cenário, utilizamos algumas formulações estatísticas.
  Para entender este processo, é preciso esclarecer alguns conceitos. Considerando
  que para as três possibilidades de votos: discordo, pular, concordo ou (-1, 0, 1),
  a chance de um usuário aleatório hipotético escolher qualquer uma delas é
  1/3, logo a soma total das probabilidades é igual a 1. Isso caracteriza um vetor
  de probabilidades discretas com três variáveis independentes, o qual podemos calcular
  as probabilidades para qualquer combinação de resultados, como a chance de tirar
  o número 6 ao rolar dois dados. Podemos representar essas probabilidades como um vetor $(\theta)$:

  \[ \theta = \left(\frac{1}{3},\frac{1}{3},\frac{1}{3}\right)\]

  Se queremos manipular essas probabilidades, causando um viés para, por exemplo,
  indicar uma maior chance de um usuário concordar com um comentário, podemos dar um peso
  maior para a probabilidade correspondente a essa variável:

  \[ \theta = \left(\frac{0.9}{3},\frac{1}{3},\frac{1.1}{3}\right)\]

  Independente do peso dado, se a soma total das probabilidades ainda for igual a 1 e nenhuma
  delas for negativa, caracterizamos um vetor de probabilidades modelado sobre
  uma distribuição de Dirichlet, que garante que seus valores são normalizados
  e positivos.

  Então, para cada grupo serão produzidas probabilidades de votos para
  os N comentários do cenário, seguindo essa distribuição, que indicam as chances
  de um usuário daquele grupo discordar, pular ou concordar com um dado comentário.
  Desta forma, cada grupo possui um conjunto de probabilidades diferentes, que
  determina a maneira como se comportam/votam os integrantes daquele grupo em
  cada comentário. A tabela \ref{table:dirichlet-probabilities} mostra
  um conjunto de probabilidades para 5 comentários que pode representar o
  comportamento de voto de um determinado grupo de participantes.

  \begin{table}[!htbp]
    \begin{minipage}{.50\textwidth}
    \begin{center}
      \caption{Probabilidades de votos.}
      \resizebox{.9\textwidth}{!}{%
      \begin{tabular}{|c |c | c | c|} \hline
      \rowcolor{lightgray}\textbf{comentário} & \textbf{$\theta$(-1)} &  \textbf{$\theta$(0)} &  \textbf{$\theta$(1)}\\\hline
       1  &  0.001&  0.256&  0.743 \\\hline
       2  &  0.825&  0.174&  0.    \\\hline
       3  &  0.395&  0.042&  0.564 \\\hline
       4  &  0.615&  0.365&  0.02  \\\hline
       5  &  0.007&  0.09 &  0.904 \\\hline
      \end{tabular}}
      \label{table:dirichlet-probabilities}
      \end{center}
    \end{minipage}
    \begin{minipage}{.50\textwidth}
      \begin{center}
      \caption{Probabilidades acumuladas.}
      \resizebox{.9\textwidth}{!}{%
      \begin{tabular}{|c |c | c | c|} \hline
        \rowcolor{lightgray}\textbf{Comentário} & \textbf{$\theta$(-1)} &  \textbf{$\theta$(0)} &  \textbf{$\theta$(1)}\\\hline
       1  &  0.001     &  0.257     &  1 \\\hline
       2  &  0.825     &  0.999     &  1 \\\hline
       3  &  0.395     &  0.437     &  1 \\\hline
       4  &  0.615     &  0.980     &  1 \\\hline
       5  &  0.007     &  0.097     &  1 \\\hline
      \end{tabular}}
      \label{table:acc-dirichlet-probabilies}
      \end{center}
    \end{minipage}
  \end{table}

  Depois de calculadas as probabilidades dos $N$ comentários para um grupo, precisamos
  gerar $K$ usuários e obter os seus votos para cada comentário. Então, para um dado usuário
  $u$ membro do grupo $G$, o qual se deseja obter os votos, geramos aleatoriamente
  um valor $x_{i}$ entre 0 e 1 seguindo uma probabilidade uniforme, relacionada
  a cada comentário $i$, tal que $i \in N$. Posteriormente calculamos
  em qual faixa de valor ela se encaixa na tabela de probabilidades acumuladas do
  grupo $G$, e isso determina qual será o voto do usuário.

  Desta forma, para calcular os votos de um usuário $V_{u}$, pertencente ao grupo
  $G$, temos:

  $ N = \text{número de comentários do cenário de teste}$

  $ i = \text{índice do comentário, tal que } i \in N$

  $ G(i) = \text{probabilidades de voto acumuladas do grupo $G$ para cada comentário $i$}$

  $ x_{i} = \text{faixa em que se situa o voto no i-ésimo comentário}$

  $ V_{u}(i) = \text{Voto do usuário $u$ no comentário $i$, sendo } V_{u}(i) = [-1, 0, 1]$

  \noindent
  Então, $\text{iniciamos } V_{u}(i) = 1 $

    $\text{se } x_{i} <  \theta(0)\text{ , definimos }V_{u}(i) = 0 $

    $\text{se } x_{i} <  \theta(-1)\text{ , definimos }V_{u}(i) = -1 $

  \begin{table}[!htbp]
    \centering
    \caption[Votos gerados para um usuário]{Votos $V_{u}$ de um usuário $u$, gerados para cada comentário $i$, segundo as
    probabilidades $\theta$ de um grupo $G$.}
    \resizebox{.5\textwidth}{!}{%
    \begin{tabular}{|c |c | c | c| c| c|} \hline
      \rowcolor{lightgray}Comentário$(i)$ & $\theta$(-1) &  $\theta$(0) &  $\theta$(1)  & $x_{i}$ & $V_{u}(i)$\\\hline
       1  &  0.001     &  0.257     &  1 & 0.213 &  0 \\\hline
       2  &  0.825     &  0.999     &  1 & 0.146 & -1 \\\hline
       3  &  0.395     &  0.437     &  1 & 0.851 &  1 \\\hline
       4  &  0.615     &  0.980     &  1 & 0.434 & -1 \\\hline
       5  &  0.007     &  0.097     &  1 & 0.111 &  1 \\\hline
    \end{tabular}}
    \label{table:acc-dirichlet-probabilies}
  \end{table}

  \noindent
  Assim, são gerados os $N$ votos para os $K$ usuários dos grupos determinados
  no cenário de teste. Como cada grupo possui probabilidades de votos diferentes, espera-se que o algoritmo
  identifique e classifique cada usuário em seu respectivo grupo.

  Utilizamos uma função da biblioteca Numpy para obter o vetor de probabilidades
pseudo aleatórias segundo a distribuição de Dirichlet:
\newline

  \pythonstyle
  \begin{lstlisting}
    >>> import numpy as np
    >>> np.random.dirichlet(alpha, size=None)
  \end{lstlisting}

  O parâmetro \textit{alpha} é um vetor $k$-dimensional, composto por números reais,
  e cada dimensão desse vetor corresponde ao peso de uma variável dentro da distribuição.
  Já o parâmetro \textit{size} indica o número de exemplos a serem gerados e simboliza
  a quantidade de comentários.

  Para produzir as probabilidades de cada opção de voto, Discordar, Pular, Concordar,
  para 5 comentários, podemos fornecer três valores em \textit{alpha} e configurar \textit{size=5}.
  Cada linha da saída corresponde a um comentário e cada valor dentro de
  um vetor representa às chances de um usuário discordar, pular e concordar com o dado comentário.
  \newline

  \lstset{language=Python}
  \begin{lstlisting}
    >>> numpy.random.dirichlet([0.8, 0.8, 0.8], 5)
    array([[ 0.15770032,  0.5041667 ,  0.33813298],
           [ 0.50116975,  0.4946626 ,  0.00416766],
           [ 0.34957988,  0.05330692,  0.5971132 ],
           [ 0.48945452,  0.24721013,  0.26333535],
           [ 0.12949316,  0.06325824,  0.8072486 ]])
  \end{lstlisting}

  Vale ressaltar que a escolha dos valores de \textit{alpha} impactam nas probabilidades geradas. Quanto mais próximo
  de 0, os valores gerados tendem a ter uma discrepância maior entre si, ou seja, cada variável
  tende a possuir probabilidades mais distantes das outras. Para valores altos de \textit{alpha}, acima de 10, a tendência
  é que as probabilidades fiquem mais igualitárias, tendendo a valores próximos de [0.33, 0.33, 0.33].
  Os números de \textit{alpha} utilizados nos testes foram [0.2, 0.4, 0.6, 0.8, 1.0],
  um para cada possível população. Se o cenário define 2 grupos, utilizamos
  [0.2, 0.2, 0.2] e [0.4, 0.4, 0.4] para gerar as probabilidades de cada grupo,
  se são 3 grupos no cenário, repetimos [0.2, 0.2, 0.2] e [0.4, 0.4, 0.4] e
  acrescentamos [0.6, 0.6, 0.6], e assim sucessivamente.

  Os valores $x_{i}$ usados para obter o voto dos usuários nos comentários também
  são dados com uma função da biblioteca Numpy, que recebe como parâmetro a quantidade
  de exemplos a serem gerados, que para nós significa o número de comentários
  no cenário:
  \newline

  \lstset{language=Python}
  \begin{lstlisting}
    >>> numpy.random.uniform(size=5)
    array([ 0.4467375, 0.9460769, 0.4989846, 0.7278234, 0.4280037 ])
  \end{lstlisting}

  Para manter a replicabilidade e coerência dos testes, é
  necessário utilizar sempre as mesmas probabilidades para cada cenário. Foram
  utilizados os mesmos valores de \textit{alpha} e também o contador utilizado pela
  linguagem para gerar números pseudo aleatórios é sempre iniciado com o mesmo
  valor antes da execução dos testes de cada cenário, conforme o código a seguir.
  \newline

  \lstset{language=Python}
  \begin{lstlisting}
    >>> numpy.random.seed(57)
  \end{lstlisting}

  \subsection{Exemplo de cenário de teste}

  Esclarecendo um pouco mais os passos para produzir os valores de referência utilizados
  para validação do algoritmo, tomemos como exemplo
  o \textbf{cenário de teste 1}, da Tabela \ref{table:cenarios-de-teste}, para o qual
  foram definidos os seguintes parâmetros:

    \begin{itemize}
      \item Número de grupos esperados = 2
      \item Número de comentários na conversa = 8
      \item Número de membros em cada grupo = 4
      \item Total de participantes = 8
    \end{itemize}

  Inicialmente, obtemos as probabilidades de voto para os dois grupos da conversa,
  segundo a distribuição de Dirichlet, vistos nas Tabelas \ref{table:dirichletGrupo0}
  e \ref{table:dirichletGrupo1}, e calculamos também os valores acumulados.

  Então, geramos as probabilidades de voto de cada usuário e calculamos
  em qual faixa de valor elas se encaixam na tabela de probabilidades acumuladas
  do seu grupo, o que resulta no valor dos votos para cada comentário, conforme
  as Tabelas \ref{table:accDirichletProbabilies0} e \ref{table:accDirichletProbabilies1}.
  \newpage

  \begin{table}[!htbp]
    \begin{minipage}{.50\textwidth}
    \begin{center}
      \caption{Probabilidades de votos grupo(0), $\alpha = 0.2$}
      \resizebox{.9\textwidth}{!}{%
      \begin{tabular}{|c |c | c | c|} \hline
        \rowcolor{lightgray}\textbf{Comentário} & \textbf{$\theta$(-1)} &  \textbf{$\theta$(0)} &  \textbf{$\theta$(1)}\\\hline
         1  &  0    &  0.168&  0.832 \\\hline
         2  &  0.898&  0.102&  0     \\\hline
         3  &  0.365&  0.013&  0.622 \\\hline
         4  &  0.990&  0.010&  0     \\\hline
         5  &  0    &  0    &  1     \\\hline
         6  &  0.132&  0.868&  0     \\\hline
         7  &  0.389&  0.611&  0     \\\hline
         8  &  0.103&  0.409&  0.488 \\\hline
         9  &  0.059&  0.564&  0.377 \\\hline
         10 &  0.993&  0.007&  0     \\\hline
      \end{tabular}}
      \label{table:dirichletGrupo0}
      \end{center}
    \end{minipage}
    \begin{minipage}{.50\textwidth}
      \begin{center}
      \caption{Probabilidades de votos grupo(1), $\alpha = 0.4$}
      \resizebox{.9\textwidth}{!}{%
      \begin{tabular}{|c |c | c | c|} \hline
        \rowcolor{lightgray}\textbf{Comentário} & \textbf{$\theta$(-1)} &  \textbf{$\theta$(0)} &  \textbf{$\theta$(1)}\\\hline
         1  &  0.367&  0.151&  0.483 \\\hline
         2  &  0.499&  0.008&  0.493 \\\hline
         3  &  0.040&  0.794&  0.166 \\\hline
         4  &  0.004&  0.814&  0.182 \\\hline
         5  &  0.850&  0.008&  0.143 \\\hline
         6  &  0.580&  0.069&  0.352 \\\hline
         7  &  0.255&  0.055&  0.690 \\\hline
         8  &  0.017&  0.972&  0.011 \\\hline
         9  &  0.798&  0.123&  0.079 \\\hline
         10 &  0.181&  0.140&  0.678 \\\hline
      \end{tabular}}
      \label{table:dirichletGrupo1}
      \end{center}
    \end{minipage}
  \end{table}

  \begin{table}[!htbp]
    \centering
    \caption{Votos (V) de 4 usuários do grupo 0.}
    \resizebox{.8\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
      \rowcolor{lightgray}Comentário & $\theta$(-1) &  $\theta$(0) &  $\theta$(1) & $V_{u}(1)$ & $V_{u}(2)$ & $V_{u}(3)$ & $V_{u}(4)$ \\\hline
       1  &  0     &  0.168 &  1 &  1  &  1  &  1&   1 \\\hline
       2  &  0.898 &  1     &  1 & -1  & -1  & -1&  -1 \\\hline
       3  &  0.365 &  0.378 &  1 &  1  & -1  &  1&   1 \\\hline
       4  &  0.990 &  1     &  1 & -1  & -1  & -1&  -1 \\\hline
       5  &  0     &  0     &  1 &  1  &  1  &  1&   1 \\\hline
       6  &  0.132 &  1     &  1 &  0  &  0  &  0&   0 \\\hline
       7  &  0.389 &  1     &  1 &  0  &  0  &  0&  -1 \\\hline
       8  &  0.103 &  0.512 &  1 &  0  &  0  &  0&   0 \\\hline
       9  &  0.103 &  0.512 &  1 &  0  &  0  &  1&   0 \\\hline
       10 &  0.103 &  0.512 &  1 & -1  & -1  & -1&  -1 \\\hline
    \end{tabular}}
    \label{table:accDirichletProbabilies0}
  \end{table}

  \begin{table}[!htbp]
    \centering
    \caption{Votos (V) de 4 usuários do grupo 1.}
    \resizebox{.8\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
      \rowcolor{lightgray}Comentário & $\theta$(-1) &  $\theta$(0) &  $\theta$(1) & $V_{u}(5)$ & $V_{u}(6)$ & $V_{u}(7)$ & $V_{u}(8)$ \\\hline
       1  &  0.367 &  0.518 &  1 & -1  & -1 &  1 & -1 \\\hline
       2  &  0.499 &  0.507 &  1 &  1  & -1 & -1 &  1  \\\hline
       3  &  0.040 &  0.834 &  1 &  0  &  1 &  0 &  0  \\\hline
       4  &  0.004 &  0.818 &  1 &  1  &  0 &  0 &  0  \\\hline
       5  &  0.850 &  0.858 &  1 & -1  & -1 & -1 & -1 \\\hline
       6  &  0.580 &  0.649 &  1 & -1  &  1 & -1 &  1  \\\hline
       7  &  0.255 &  0.310 &  1 & -1  &  1 & -1 &  1  \\\hline
       8  &  0.017 &  0.989 &  1 &  0  &  0 &  0 &  0  \\\hline
       9  &  0.798 &  0.921 &  1 & -1  & -1 & -1 & -1 \\\hline
       10 &  0.181 &  0.321 &  1 & -1  &  1 &  0 &  0  \\\hline
    \end{tabular}}
    \label{table:accDirichletProbabilies1}
  \end{table}

  De acordo com as características desse cenário, esperamos então que o algoritmo
  classifique os 8 usuários em 2 grupos diferentes, o grupo 0 e o grupo 1.
  Para verificar a taxa de acerto do modelo, o teste escrito para este cenário
  cria uma conversa com 10 comentários, insere os votos gerados para os usuários
  e executa a clusterização com este conjunto de votos, calculando ao final, o
  índice de Rand ajustado entre os resultados esperados e obtidos.

  \noindent
  \textbf{Resultado esperado:} [0, 0, 0, 0, 1, 1, 1, 1]

  \noindent
  \textbf{Resultado obtido:} [1, 1, 1, 1, 0, 2, 0, 2]

  \noindent
  \textbf{Índice de Rand Ajustado:} 0.7

  Vejamos, neste exemplo, que o algoritmo classificou os participantes em 3
  clusters. O cluster 0 esperado corresponde ao cluster de número 1 nos resultados
  obtidos e o cluster 1 foi dividido nos clusters 0 e 2. O valor 0.7 do índice indica que, para cada par de objetos selecionado, existe
  uma probabilidade de 70\% deles estarem agrupados igualmente
  nas duas classificações. O número de clusters ou a permutação dos rótulos(número do grupo)
  não afeta no cálculo do índice. Na próxima seção apresentamos e discutimos os resultados para cada um dos cenários
  de teste propostos.

\section{Resultados e discussões}

  Buscando uma maior confiança na operação do sistema, desenvolvemos testes
  automatizados para as classes e métodos implementados. Para isso, utilizamos a
  biblioteca de testes Pytest. Estes testes auxiliaram tanto no desenvolvimento
  do algoritmo quanto na avaliação da acurácia do modelo após a sua implementação.
  Para cada um dos cenários definidos na Tabela \ref{table:cenarios-de-teste},
  escrevemos um teste para medir o índice de Rand ajustado entre a saída esperada
  e a saída obtida, capturando os resultados dos testes de acurácia para todos
  os 14 cenários definidos. Esses resultados são vistos na Tabela \ref{table:resultados}.


  \begin{table}[!htbp]
    \caption{Dados de cada cenário e acurácia obtida.}
    \centering
    \resizebox{1\textwidth}{!}{%
    \begin{tabular}{|c | c | c| c| c| c| c| c| c| c| c|} \hline
      \rowcolor{lightgray} Cenário &  N comentários  &  N membros/grupo &  Total participantes & N grupos esperados & Grupos gerados & Acurácia\\\hline
      1         &  8              &  4               &  8  &  2  &  3  &  0.69 \\\hline
      2         &  50             &  5               &  10 &  2  &  2  &  1.0  \\\hline
      3         &  50             &  10              &  20 &  2  &  2  &  1.0  \\\hline
      4         &  150            &  10              &  20 &  2  &  2  &  1.0  \\\hline
      5         &  8              &  4               &  8  &  3  &  4  &  0.87 \\\hline
      6         &  50             &  5               &  15 &  3  &  3  &  1.0  \\\hline
      7         &  50             &  10              &  30 &  3  &  3  &  1.0  \\\hline
      8         &  150            &  10              &  30 &  3  &  3  &  1.0  \\\hline
      9         &  50             &  5               &  20 &  4  &  4  &  1.0  \\\hline
      10        &  50             &  10              &  40 &  4  &  3  &  0.69 \\\hline
      11        &  150            &  10              &  40 &  4  &  4  &  1.0  \\\hline
      12        &  50             &  5               &  25 &  5  &  4  &  0.75 \\\hline
      13        &  50             &  10              &  50 &  5  &  3  &  0.51 \\\hline
      14        &  150            &  10              &  50 &  5  &  4  &  0.76 \\\hline
      \rowcolor{lightgray} Média   &  \multicolumn{5}{c|}{}                 &  0.87 \\\hline
    \end{tabular}}
    \label{table:resultados}
  \end{table}

  Na média, o modelo obteve uma acurácia de 87\%. Para entendermos os pontos
  onde o resultado obtido foi diferente do esperado, mostramos os dados em
  gráficos para compreender de forma visual cada cenário, fazendo um contraste
  entre os dados classificados manualmente com as probabilidades
  da distribuição de  Dirichlet e o resultado gerado pelo algoritmo.

   \begin{figure}[!hbt]
     \centering
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-1-esperado.eps}
       \caption{Cenário 1 esperado.}
     \label{fig:cenario-1-esperado}
     \end{minipage}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-1-obtido.eps}
       \caption{Cenário 1 obtido.}
     \label{fig:cenario-1-obtido}
     \end{minipage}
   \end{figure}

   O cenário 1 acima, era o mais simples, com menor quantidade de comentários,
   grupos e participantes. A Figura \ref{fig:cenario-1-esperado}, que representa a classificação feita
   manualmente com o método das probabilidades, mostra que acabamos obtendo
   alguns valores de voto muitos distantes para membros do mesmo grupo, como podemos
   notar nos pontos em vermelho bastante dispersos. Com isso o algoritmo identificou-os
   como sendo dois grupos distintos e a acurácia foi de 69\%.

   \begin{figure}[!hbt]
     \centering
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-5-esperado.eps}
       \caption{Cenário 5 esperado.}
     \label{fig:cenario-5-esperado}
     \end{minipage}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-5-obtido.eps}
       \caption{Cenário 5 obtido.}
     \label{fig:cenario-5-obtido}
     \end{minipage}
   \end{figure}

   O cenário 5, teve os mesmos efeitos do primeiro, mas com apenas 1 ponto
   fora do esperado. Aqui, o algoritmo identificou um cluster com um membro isolado,
   o que é perfeitamente plausível. Foi calculado nesse cenário uma acurácia foi de 87\%
   \newpage

   \begin{figure}[!hbt]
     \centering
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-10-esperado.eps}
       \label{fig:cenario-10-esperado}
     \caption{Cenário 10 esperado.}
     \end{minipage}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-10-obtido.eps}
       \label{fig:cenario-10-obtido}
     \caption{Cenário 10 obtido.}
     \end{minipage}
   \end{figure}

   O cenário 10, teve um efeito contrário aos cenários 1 e 5. Nesse caso de teste,
   as probabilidades geradas para dois grupos foram muito próximas uma das outras (pontos vermelhos e
   azuis) e o algoritmo as identificou como sendo um grupo só, o que resultou em uma
   acurácia de 69\%.

   \begin{figure}[!hbt]
     \centering
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-12-esperado.eps}
       \label{fig:cenario-12-esperado}
     \caption{Cenário 12 esperado.}
     \end{minipage}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-12-obtido.eps}
       \label{fig:cenario-12-obtido}
       \caption{Cenário 12 obtido.}
     \end{minipage}
   \end{figure}

   No cenário 12, a proximidade entre as probabilidades de dois grupos também foi
   muito alta, havendo até uma superposição de pontos. O algoritmo também acabou juntando
   dois clusters em um, como no cenário 10. Nesse cenário, a acurácia foi de 75\%.
   A medida que cresce o número de comentários, participantes e grupos esperados das
   conversas, fica mais difícil identificar todos os clusters corretamente, pois
   a taxa de probabilidades parecidas e até repetidas entre os grupos aumenta.
   \newpage

   \begin{figure}[!hbt]
     \centering
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-13-esperado.eps}
       \label{fig:cenario-13-esperado}
     \caption{Cenário 13 esperado.}
     \end{minipage}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-13-obtido.eps}
       \label{fig:cenario-13-obtido}
     \caption{Cenário 13 obtido.}
     \end{minipage}
   \end{figure}

   \begin{figure}[!hbt]
     \centering
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-14-esperado.eps}
       \label{fig:cenario14esperado}
     \caption{Cenário 14 esperado.}
     \end{minipage}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/cenario-14-obtido.eps}
       \label{fig:cenario14obtido}
     \caption{Cenário 14 obtido.}
     \end{minipage}
   \end{figure}

    Nos testes para os cenários 13 e 14, a superposição foi mais acentuada, o que
    reforça a necessidade de ajuste nos valores das probabilidades geradas.
    Aqui, a acurácia caiu abruptadamente para 51\% e 76\%, respectivamente.

    Podemos ver que os resultados gerados pelo algoritmo são mais coerentes que
    os gerados através das probabilidades. Os resultados dos cenários 12, 13 e 14 mostram que esses foram os cenários mais
    afetados pelas probabilidades similares geradas, com algumas superposições
    entre os grupos, levando o algoritmo a identificar esses pontos como
    sendo do mesmo cluster, o que faz total sentido.

    Apesar dos erros, o método de validação através das probabilidades demonstrou ser
    uma boa saída para formulação da base de dados de teste caso seja implementada
    uma pequena melhoria que evitaria a geração de probabilidades parecidas.
    Podemos estabelecer um limiar mínimo para a diferença entre
    a distância probabilística entre os valores gerados para cada grupo, que pode ser
    calculado como um somatório da diferença entre as probabilidades.

    \[ \frac{1}{2} \sum(P_{i} - Q_{i}) \]

    Todavia, analisando as imagens da clusterização feita pelo algoritmo já é
    possível perceber que o modelo possui um nível de coerência maior do que o
    refletido nos números, separando bem os clusters.
