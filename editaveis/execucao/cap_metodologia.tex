\chapter{Proposta de solução}
\label{chapter:5}
  Até este capítulo do trabalho foram apresentados diversos termos, técnicas e algoritmos
  existentes na literatura de clusterização de dados. Com este estudo feito, utilizaremos
  este conhecimento técnico para buscar responder então à seguinte pergunta: Como
  identificar grupos de opinião em uma conversa entre usuários?

  Este capítulo apresenta como será desenvolvida a solução que responde à
  dada pergunta. Passamos primeiramente pelo tópico que diz
  respeito à representação dos itens e definição de seus atributos. Em seguida,
  trataremos da fase de pré-processamento dos dados e posteriormente a escolha
  de um algoritmo de análise de clusters que identifique os grupos de opinião
  almejados. 

  Para criar a aplicação que contempla nossa proposta, utilizaremos o
  \textit{framework} Django\footnote{\url{djangoproject.com}}
  e a biblioteca científica Scikit-learn\footnote{\url{scikit-learn.org}},
  que oferece diversos algoritmos de aprendizado de máquina. Elas são ferramentas
  livres, escritas na linguagem Python e irão facilitar no desenvolvimento.

\section{Representação dos usuários}
  Para representar os usuários do sistema e posteriormente identificar os grupos
  com base nos votos, consideramos então os itens/objetos como sendo os usuários
  participantes e os comentários da conversa como sendo os atributos de cada usuário.
  Para o valor de cada atributo, utilizaremos a faixa de limites $[-1,0,1]$,
  correspondentes às possíveis escolhas de interação pelos usuários.

  \begin{table}[!htbp]
    \label{table:votes-options}
    \begin{center}
      \begin{tabular}{|c c c|} \hline
        \rowcolor{lightgray}Discordo & Pular/Não Informado & Concordo\\\hline
        -1 & 0 & 1\\\hline
      \end{tabular}
    \end{center}
  \end{table}

  Assim, podemos construir uma matriz ``participantes $\times$ comentários'' com os valores dos
  votos de cada participante, resultando em um conjunto de dados $N$-dimensional
  que é dependente do número de comentários e participantes na conversa, como pode
  ser visto na Tabela \ref{table:votesXcomments}, a seguir:

  \begin{table}[!htbp]
    \begin{center}
      \caption[Tabela de votos de usuários em comentários]{Tabela de votos dos
      participantes (P) em comentários (C) de uma conversa.}
      \resizebox{.4\textwidth}{!}{%
      \begin{tabular}{|c | c | c| c| c| c| c| c|} \hline
       \rowcolor{lightgray}  &  C1 &  C2 &  C3 &  C4 &  C5 & ... &  Cn\\\hline
        P1  &  0  &  1  & -1  & -1  & -1  & ... & -1\\\hline
        P2  &  1  &  0  &  0  &  1  &  1  & ... &  1\\\hline
        P3  &  1  &  1  & -1  &  0  &  1  & ... &  0\\\hline
        P4  & -1  &  1  &  1  &  0  &  1  & ... &  1\\\hline
        P5  &  1  &  1  &  1  &  0  &  1  & ... &  1\\\hline
        PN  &  1  &  1  & -1  &  1  &  0  & ... &  0\\\hline
      \end{tabular}}
    \label{table:votesXcomments}
    \end{center}
  \end{table}

  Um importante ponto a favor sobre esta representação é que ela em si, contém toda
  a informação necessária para fazer a análise, dispensando
  ajustes de escala ou tratamento adicional de dados ausentes, já que consideramos
  como zeros os casos em que não há votos do usuário em algum dos comentários.

  Vale ressaltar que a ausência de dados pode afetar diretamente no resultado
  do agrupamento, pois seu valor impacta na escolha e no cálculo das distâncias
  entre os objetos. A escolha desta representação foi inspirada na implementação
  da plataforma Polis e nos critérios (Seção \ref{criterios-ej}) do projeto Empurrando Juntos.

\section{Pré-processamento e transformação dos dados}
  O critério \ref{criterios-ej-3}, descrito na Seção \ref{criterios-ej}, define que:
       ``O novo serviço de clusterização deve ser capaz de identificar grupos de
       opinião em uma conversa com base nos votos dos usuários, que possam ser
       visualizados em gráficos similares aos da plataforma Polis.''
  Este critério, afirma uma necessidade de visualização dos agrupamentos
  em um gráfico de duas dimensões. Visto que o conjunto de dados
  é representado em múltiplas dimensões, proporcional ao número de comentários,
  faz-se necessário uma transformação de redução da dimensionalidade destes
  para algo tangível à percepção humana. Nesse sentido, optamos por aplicar
  uma redução também para 2 dimensões, utilizando a técnica PCA, descrita na Seção
  \ref{data-transformation}.

  Para uma melhor qualidade dos resultados, seria ideal executar a clusterização
  utilizando todas as dimensões dos dados. Porém, no momento da visualização em
  duas dimensões, os dados estariam suscetíveis à superposição dos clusters.
  Esse problema pode acontecer quando há um cluster ``atrás de outro'' em uma dimensão
  extra não mostrada na projeção 2D, como pode ser observado na Figura
  \ref{fig:superposicao}, onde os centros dos clusters estão distorcidos após
  aplicar o PCA nos dados já clusterizados:

  Por isso, aplicaremos a transformação de redução anteriormente ao cálculo dos clusters.
  Sabemos também, que esta redução gera outros impactos no sistema. O principal deles,
  é uma distorção que acarreta em uma certa perda
  de informação, já que se trata de uma projeção dos dados originais.
  Além disso, haverá ainda uma mudança na escala dos dados. Uma vez que o valores estavam
  limitados aos dígitos [-1, 0, 1], após a transformação
  eles se tornam elementos em uma faixa contínua, o que impacta diretamente na
  escolha do algoritmo e da função de distância a serem utilizados. As Tabelas
  \ref{tabletinho} e \ref{tabletao} mostram uma transformação
  dos dados usando o PCA.

   \begin{figure}[!hbt]
     \begin{center}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/superposicao.eps}
       \caption{Distorção dos centros.}
       \label{fig:superposicao}
     \end{minipage}
     \begin{minipage}{.49\textwidth}
       \includegraphics[width=1\linewidth]{figuras/sem-superposicao.eps}
       \caption{Sem superposição.}
       \label{fig:sem-superposicao}
     \end{minipage}
     \end{center}
   \end{figure}

  \begin{table}[!htbp]
    \begin{minipage}{.5\linewidth}
      \begin{center}
        \caption{Conjunto original.}
        \begin{tabular}{|c | c | c| c| c| c| c| c| c|} \hline
          \rowcolor{lightgray}  &  C1 &  C2 &  C3 &  C4 &  C5 &  C6 &  C7 &  C8\\\hline
          P1  &  0  &  1  & -1  & -1  & -1  & -1  & -1  & -1\\\hline
          P2  &  1  &  0  &  0  &  1  &  1  &  1  &  1  &  1\\\hline
          P3  &  1  &  1  & -1  &  0  &  1  &  0  &  1  &  1\\\hline
          P4  & -1  &  1  &  1  &  0  &  1  &  0  & -1  &  0\\\hline
          P5  &  1  &  1  &  1  &  0  &  1  &  1  & -1  &  0\\\hline
        \end{tabular}
        \label{tabletinho}
      \end{center}
    \end{minipage}
    \begin{minipage}{.5\linewidth}
      \begin{center}
        \caption{Conjunto reduzido.}
        \begin{tabular}{|c | c | c| c|} \hline
          \rowcolor{lightgray}  &  D1 &  D2 \\\hline
          P1  &  2.59  & -1.18 \\\hline
          P2  & -2.08  & -0.26 \\\hline
          P3  & -1.12  & -1.38 \\\hline
          P4  &  0.81  &  1.51 \\\hline
          P5  & -0.20  &  1.32 \\\hline
        \end{tabular}
        \label{tabletao}
      \end{center}
    \end{minipage}
  \end{table}

  O PCA acaba descartando os comentários com poucas respostas, já que agregam uma baixa
variância ao conjunto de dados. Aplicando a redução, obteremos um melhor desempenho
do algoritmo de clusterização, já que utiliza-se apenas duas dimensões, o que otimiza
o cálculo das distâncias entre os pontos, na verificação das similaridades.

\section{Modelo de clusters}
  Após a representação dos objetos, ponto de partida para a implementação de
  qualquer modelo de clusterização, o próximo passo é escolher o algoritmo que
  irá executar a tarefa. O algoritmo para a ferramenta Empurrando Juntos,
  deve considerar que novos usuários comentam e votam a todo momento em uma dada
  conversa, processando um conjunto de dados de N participantes que votaram em N comentários.
  A Figura \ref{fig:resumo-ej} ilustra como se dá esse fluxo.
  \newpage

  \begin{figure}[h]
    \centering
    \resizebox{.6\textwidth}{!}{%
    \includegraphics[scale=1, center]{figuras/resumo-ej.eps}}
    \caption[Fluxo de participantes em uma conversa\protect\footnote{Imagem retirada do Trabalho de Conclusão de Curso de Emilie T. de Morais e Ítalo P. Batista}]{Fluxo de participantes em uma conversa.}
    \label{fig:resumo-ej}
  \end{figure}


  Dado o contexto e os critérios definidos na seção \ref{criterios-ej}, e
  também características citadas dos modelos baseados em centroide,
  será aplicado o algoritmo \textit{k-means} em conjunto com a técnica de silhueta
  para a escolha do melhor resultado. Assim, o modelo responderá a cada novo voto
  em uma conversa, considerando um $k$ máximo de até 5 grupos na conversa e irá retornar
  o cenário de maior coeficiente de silhueta, o que indicaria a melhor divisão dos clusters.
  Esse valor de $k$ foi determinado pensando-se na visualização dos grupos,
  mas certamente irá passar por refinamentos futuros.

  Tratando-se da função de similaridade que será utilizada para aferir a aproximação
  entre os usuários, escolhemos a equação Euclidiana simples, vista na
  Seção \ref{sec:medidas-de-similaridade}.
  Essa escolha, por sua vez, se encaixa com a transformação feita nos dados com
  a técnica PCA, visto que os dados transformados passam a ser numéricos e não
  mais categóricos, e também porque não há uma diferença de escala entre o valor das variáveis,
  sendo este um cenário plausível para a utilização desta medida.

  A Figura \ref{fig:resumo-clusterizacao-ej} representa o esquemático do modelo.
  O fluxo começa a partir de novos votos em comentários da conversa, que disparam o
  algoritmo de clusterização. O $k$-means é executado 5 vezes com os parâmetros
  de $k$ igual a 1, 2, 3, 4 e 5 e o coeficiente de silhueta é calculado para cada
  uma das execuções. Ao final, o particionamento de maior coeficiente é salvo
  como resultado.

  \newpage

   \begin{figure}[!ht]
     \centering
     \includegraphics[scale=.55, center]{figuras/resumo-clusterizacao-ej.eps}
     \caption[Ilustração do modelo\protect\footnote{Imagem retirada do Trabalho de Conclusão de Curso de curso Emilie T. de Morais e Ítalo P. Batista}]{Ilustração do modelo.}
     
     \label{fig:resumo-clusterizacao-ej}
   \end{figure}

   Tanto o algoritmo $k$-means, quanto o coeficiente de silhueta são implementados
   na biblioteca Scikit-learn e serão utilizados para construir o modelo. A vantagem
   de se utilizar uma biblioteca madura e amplamente testada é que podemos ter uma
   maior confiança nos seus resultados e na sua eficiência.

