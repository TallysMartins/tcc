\begin{apendicesenv}

\partapendices

 \chapter{Medidas de distância}
\label{apendice:medidas}

  Neste apêndice apresentamos algumas outras medidas de similaridade para dados
numéricos, categóricos e binários. \citeonline{SIAM2007} cita ainda outras medidas
que foram desenvolvidas para outros contextos de dados, como atributos temporais,
imagens, sons, contextos mistos, e mais.

  \section{Medidas para dados numéricos}
  \label{sec:medidas-numericas}

  \begin{itemize}
  \item{\underline{Distância Média:}} Esta é uma versão modificada da distância Euclidiana.
Esta função acaba sendo mais intuitiva, pois distribui o impacto dos
atributos dividindo o resultado pela quantidade total destes, gerando uma média.
Ela minimiza possíveis disparidade dos valores dos atributos em conjuntos de dados
onde há valores ausentes para alguns atributos ou com grandes diferenças de
escala entre eles.

  \[ \dis(x,y) = \sqrt{\frac{1}{N}\sum_{k=1}^N (x_k - y_k)^2} \]

  \item{\underline{Distância Euclidiana com pesos:}} Se existe alguma ponderação diferente
para os atributos, esta medida pode ser usada. Seu cálculo aplica a relevância
individual de cada propriedade do objeto.

  \[ \dis(x,y) = \sqrt{\sum_{k=1}^N w_k(x_k - y_k)^2} \]

  \item{\underline{Similaridade do Cosseno:}} Esta medida é comumente utilizada para calcular distâncias entre vetores de
documentos de texto \cite{Shirkorshidi2015}. Nesta definição, $\mid\mid{x}\mid\mid_2$ e $\mid\mid{y}\mid\mid_2$
representam as normas dos vetores ${x} = (x_1, x_2,...,x_n)$ e ${x} = (y_1, y_2,...,y_n)$,
dadas por:  $\mid\mid{x}\mid\mid_2 = \sqrt{x_1^2 + x_2^2 + x_n^2}$.
% REF A Comparison Study on Similarity and Dissimilarity Measures in Clustering Continuous Data

  \[ \disCos(x,y) = \frac{\sum_{k=1}^N x_k y_k}{\mid\mid{x}\mid\mid_2 \mid\mid{y}\mid\mid_2} \]
  \end{itemize}

  \section{Medidas para dados categóricos}

  Atributos categóricos são também comumente chamados de atributos nominais, os
quais simplesmente utilizam nomes, como a cor de um objeto ou a marca de um carro.
Esse tipo de informação é analisada de forma diferente de dados numéricos contínuos,
já que sua natureza não é quantificável.

  \begin{itemize}
  \item{\underline{Distância de correspondência simples:}} Esta é a distância mais
simples e conhecida para medida de atributos categóricos, onde é simplesmente
feita uma contagem da quantidade de itens iguais e diferentes entre os objetos.
A distância de dois objetos $x$ e $y$ para um atributo $k$, representada por
$\delta$, é dada por:

  \[
    \delta(x_k, y_k) = \begin{cases}
                       0 \text{ se } x_k = y_k, \\
                       1 \text{ se } x_k \neq y_k
                       \end{cases}
  \]

  Assim, seja um vetor $d$-dimensional de atributos para os objetos $x$ e $y$,
a distância de simples correspondência é calculada pela soma dos $\delta$ de cada
um deles, e quanto maior o valor, mais distantes são os objetos:

  \[ \dis(x,y) = {\sum_{k=1}^d \delta(x_k,y_k)} \]

  No entanto, podemos normalizar o resultado para um intervalo entre [0 - 1) da seguinte
forma:

  \[ \dis(x,y) = 1 - \frac{1}{1 +\sum_{k=1}^d \delta(x_k,y_k)} \]

  Note que o resultado será igual a zero para objetos idênticos, onde $\delta = 0$,
mas nunca será igual a 1 para objetos totalmente distintos.
\newline

  \item{\underline{Outras medidas de correspondência:}} Além da disância de correspondência
simples, existem outros possíveis coeficientes para dados categóricos. Vejamos
então, três definições prévias para o seu entendimento:
\newline

  $N_{a+d} \Rightarrow$ Soma total do número de atributos correspondentes
entre dois objetos.

  \[ N_{a+d} = {\sum_{k=1}^d [1 - \delta(x_k,y_k)]} \]

  $N_{d} \Rightarrow$ Soma total do número de atributos indefinidos. Algumas
medidas contabilizam a falta de informação no cálculo da distância, e o valor
que representa este dado é de escolha da aplicação, podendo ser 0, ``indefinido'', -1, ``nulo'', etc.

  \[ N_{d} = {\sum_{k=1}^d [\delta(x_k, nulo) + \delta(y_k, nulo) - \delta(x_k, nulo) \times \delta(y_k, nulo) ]} \]

  $N_{b+c} \Rightarrow$ Soma total do número de atributos não correspondentes
entre dois objetos.

  \[ N_{b+c} = {\sum_{k=1}^d \delta(x_k,y_k)} \]

  Dadas as definições, podemos então entender algumas das medidas de similaridade,
sumarizadas na Tabela \ref{table:categorical-measures}:

  \begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Medida} & \textbf{s(x,y)} & \textbf{Peso das correspondências e não correspondências} \\ \hline
		Russel and Rao  & $\frac{N_{a+d} - N_{d}}{N_{a+d} + N_{b+c}}$ & Pesos iguais                                              \\ \hline
		Simple matching & $\frac{N_{a+d}}{N_{a+d} + N_{b+c}}$  & Pesos iguais                                              \\ \hline
		Jaccard         &  $\frac{N_{a+d} - N_{d}}{N_{a+d} - N_{d} + N_{b+c}}$ & Pesos iguais                                              \\ \hline
		Dice            &  $\frac{2N_{a+d} - 2N_{d}}{2N_{a+d} - 2N_{d} + N_{b+c}}$ & Dobro do peso para pares correspondentes                  \\ \hline
		Rogers-Tanimoto &  $\frac{N_{a+d}}{N_{a+d} + 2N_{b+c}}$ & Dobro do peso para pares não correspondentes              \\ \hline
		Kulczynski      & $\frac{N_{a+d} - N_{d}}{N_{b+c}}$ & Pares correspondentes excluídos do denominador            \\ \hline
	\end{tabular}
	\caption[Algumas medidas de similaridade para dados nominais.]{Algumas medidas de similaridade para dados nominais.
  \newline Fonte: \citeonline{SIAM2007}, adaptado}
	\label{table:categorical-measures}
\end{table}

  \end{itemize}

  \section{Medidas para dados binários}

  Existem também medidas específicas para o cálculo da distância para contextos
onde os objetos são representados por características binárias, ou seja, só são
possíveis dois valores, como ``verdadeiro'' e ``falso''. Essa categoria de atributos
pode ainda ser dividida em duas classes, binários simétricos e assimétricos.
Valores simétricos representam dados com peso igualmente importantes, como ``masculino''
e ``feminino''. Já a classe de valores assimétricos tratam valores onde um possui
mais relevância que o outro, como ``sim'' para a presença de uma determinada
condição e ``não'' para sua ausência. Esses atributos são uma especialidade de
dados categóricos e podem até ser calculados com as mesmas medidas. Mas também,
possuem suas medidas específicas que foram propostas para seu contexto.

Assim, sejam ${x}$ e ${y}$ dois objetos representados por um vetor em um espaço ${d}$-dimensional,
e sejam ${A, B, C, D}$ definidos como a soma $S$ da quantidade de correspondências dos valores em
$x$ e em $y$ para as combinações possíveis em cada atributo de um objeto, e
$\alpha$ como sendo uma relação dessas correspondências:

  \[A = S_{11}(x,y), \text{soma dos elementos tal que $x_{i} = 1$ e $y_{i} = 1$} \]
  \[B = S_{10}(x,y), \text{soma dos elementos tal que $x_{i} = 1$ e $y_{i} = 0$}\]
  \[C = S_{01}(x,y), \text{soma dos elementos tal que $x_{i} = 0$ e $y_{i} = 1$}\]
  \[D = S_{00}(x,y), \text{soma dos elementos tal que $x_{i} = 0$ e $y_{i} = 0$}\]
  \[\alpha = \sqrt{(A+B)(A+C)(B+D)(C+D)}\]
  \newline

  Então, sobre essas definições podemos observar alguma dessas medidas para esse tipo
de dado, vide a Tabela \ref{table:bin-measures}.

  \begin{table}[!h]
  	\centering
  	\begin{tabular}{|c|c|c|}
  		\hline
  		\textbf{Medida}   & \textbf{s(x,y)} & \textbf{Limite de s(x,y)} \\ \hline
  		Jaccard           & $\frac{A}{A + B + C}$ & $[0, 1]$  \\ \hline
  		Dice              & $\frac{A}{2A + B + C}$ & $[0, \frac{1}{2}]$  \\ \hline
  		Pearson           & $\frac{AD - BC}{\sigma}$ & $[-1, 1]$  \\ \hline
  		Yule              & $\frac{AD - BC}{AD + BC}$ & $[-1, 1]$  \\ \hline
  		Russell-Rao        & $\frac{A}{d}$ & $[0, 1]$  \\ \hline
  		Sokal-Michener    & $\frac{A + D}{d}$ & $[0, 1]$  \\ \hline
  		Rogers-Tanimoto   & $\frac{A + D}{A + 2(B + C) + D}$ & $[0, 1]$  \\ \hline
  		Rogers-Tanimoto-a & $\frac{A + D}{A + 2(B + C) + D}$ & $[0, 1]$  \\ \hline
  		Kulzinsky         & $\frac{A}{B + C}$ & $[0, \infty]$  \\ \hline
  	\end{tabular}
    \caption[Algumas medidas de similaridade para dados binários.]{Algumas medidas de similaridade para dados binários.
    \newline Fonte: \citeonline{SIAM2007}, adaptado}
  	\label{table:bin-measures}
  \end{table}


  Muitas medidas de similaridade são descritas na literatura, o que causa uma
certa dúvida e confusão na hora de escolher a solução apropriada. A avaliação
das medidas é um tanto subjetiva e há várias maneiras de comparar os resultados
de cada uma. \citeonline{Shirkorshidi2015} apresentam um interessante estudo
de comparação entre diversas funções em diferentes conjuntos de dados com
atributos numéricos contínuos. Nesse estudo são utilizados dados de baixa e
de alta dimensionalidade advindos dos mais variados contextos. A conclusão,
no entanto, revela que não há uma medida que resolva todos os casos, cada
contexto exige uma análise particular para um resultado aprimorado.

\chapter{Técnicas}
\label{apendice:técnicas}

\section{Análise de Componentes Principais - PCA}
\label{apendice:pca}

  Esta é uma técnica é comumente utilizada para transformação dos dados com o objetivo de
  reduzir sua dimensão, quando há um grande número de variáveis nos dados, o que
  pode causar o problema da maldição da dimensionalidade.
  Elucidamos a seguir os passos para aplicação do PCA para reduzir um conjunto
  de dados de duas para uma dimensão. A Tabela \ref{table:pca-dataset} abaixo mostra 20 objetos relacionados
  para as variáveis X1 e X2.
  \newline

  \begin{table}[!htbp]
    \begin{center}
    \resizebox{.7\linewidth}{!}{%
    \begin{tabular}{|c | c | c| c| c| c| c| c| c| c| c|} \hline
\rowcolor{lightgray} Objetos &  X1  &  X2   & Objetos &  X1  & X2   \\\hline
                      1      &  0.3 &  0.5  &  11     &  0.6 &  0.8 \\\hline
                      2      &  0.4 &  0.3  &  12     &  0.4 &  0.6 \\\hline
                      3      &  0.7 &  0.4  &  13     &  0.3 &  0.4 \\\hline
                      4      &  0.5 &  0.7  &  14     &  0.6 &  0.5 \\\hline
                      5      &  0.3 &  0.2  &  15     &  0.8 &  0.5 \\\hline
                      6      &  0.9 &  0.8  &  16     &  0.8 &  0.9 \\\hline
                      7      &  0.1 &  0.2  &  17     &  0.2 &  0.3 \\\hline
                      8      &  0.2 &  0.5  &  18     &  0.7 &  0.7 \\\hline
                      9      &  0.6 &  0.9  &  19     &  0.5 &  0.5 \\\hline
                      10     &  0.2 &  0.2  &  20     &  0.6 &  0.4 \\\hline
    \end{tabular}}
    \caption{Conjunto de dados}
    \label{table:pca-dataset}
    \end{center}
  \end{table}

  Então, como primeiro passo para a análise dos componentes principais,
  centralizamos os dados em torno dos eixos subtraindo a média de cada
  variável no conjunto de pontos, como mostra a figura \ref{fig:pca-dataset-centered}.
  \newpage

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.9\linewidth]{figuras/pontos.eps}
    \caption{Pontos plotados}
    \label{fig:pca-dataset}
  \end{figure}

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.9\linewidth]{figuras/pontos-centralizados.eps}
    \caption{Pontos centralizados}
    \label{fig:pca-dataset-centered}
  \end{figure}
  \newpage

  Após a translação do eixo, calculamos a matriz de covariância entre as variáveis,
  o quenos dá informações sobre a relação que elas possuem. Se a covariância é positiva,
  então as duas variáveis crescem e decrescem juntas. Se seu valor é negativo,
  então enquanto uma variável cresce, a outra decresce e vice-versa. Esses
  valores determinam a dependência linear entre as variáveis que serão
  usadas para redução da dimensão dos dados, como mostra a tabela
  \ref{tab:covariance-matrix}.

  \begin{table}[!htbp]
    \begin{center}
      \begin{tabular}{|p{2cm}|p{2cm}|p{2cm}|} \hline
        -  & \textbf{X1} & \textbf{X2} \\\hline\hline
        \textbf{X1} & 0.33 & 0.25 \\\hline
        \textbf{X2} & 0.25 & 0.41 \\\hline
      \end{tabular}
      \caption{\label{tab:covariance-matrix}Matriz de covariância entre as variáveis}
    \end{center}
  \end{table}

  Os valores da diagonal principal representam a covariância de cada variável
  com ela mesma, o que é igual a sua medida de variância. A variância mede o quão dispersos
  os dados estão em relação a média. A diagonal secundária mostra a covariância entre
  as duas variáveis. Os valores positivos mostram que as duas crescem e decrescem
  juntas.

  Podemos então encontrar os autovetores e autovalores da matriz de covariância.
  Como o conjunto possui duas dimensões, variáveis X1 e X2, o número de autovetores será igual a 2.
  A figura \ref{fig:eigenvectors} representa os autovetores do exemplo:

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.9\linewidth]{figuras/pontos-autovetores.eps}
    \caption{Autovetores dos dados}
    \label{fig:eigenvectors}
  \end{figure}
  \newpage

  Os autovetores calculados a partir da matriz de covariância representam as direções
  em que os dados acumulam a maior variância. Já os respectivos autovelores determinam
  a quantidade de variância naquela direção. Uma vez que obtemos os autovetores,
  podemos selecionar aqueles onde serão projetados os dados. Os autovetores selecionados
  são chamados componentes principais.

  O critério para seleção dos autovetores é o total de variância que ele representa.
  Como já foi dito, essa variância é representada pelo autovalor de cada autovetor.
  A soma dos autovalores é igual a variância total dos dados. Se queremos projetar
  os dados para 1 dimensão, selecionamos o autovetor que possui o maior autovalor.

  A tabela \ref{tab:eigenvalues} mostra os autovalores para os autovetores
  calculados. Podemos observar que o primeiro autovetor representa cerca de 85\%
  da variância dos dados, enquanto o segundo explica cerca de 15\%. Os valores
  acumulados somam a variância total.

  \begin{table}[!htbp]
    \begin{center}
      \begin{tabular}{|p{2cm}|p{5cm}|p{5cm}|} \hline
        -  & \textbf{Autovalores/Variância} & \textbf{Variância acumulada} \\\hline\hline
        \textbf{1} & 84.5979 & 84.5979 \\\hline
        \textbf{2} & 15.4021 & 100 \\\hline
      \end{tabular}
      \caption{Autovalores dos autovetores}
      \label{tab:eigenvalues}
    \end{center}
  \end{table}

  Uma maneira comum de selecionar os autovetores é estabelecer um valor limite
  de variância desejada para o resultado. Por exemplo, se queremos manter 90\%
  da representatividade, selecionamos quantos componentes quanto necessários para
  obter este valor, que é o acumulado da soma das variâncias de cada componente.
  Como neste exemplo iremos aplicar a redução para 1 dimensão, será selecionado
  o primeiro autovetor como componente principal. Por consequente, o resultado final
  terá 85\% da representatividade dos dados originais.

  Selecionados os componentes, o último passo é projetar os dados nas novas
  coordenadas. A figura \ref{fig:eigenvectors-result-projection} mostra o resultado
  desta projeção.

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.9\linewidth]{figuras/pca-projection.eps}
    \caption{Projeção dos dados no componente selecionado}
    \label{fig:eigenvectors-result-projection}
  \end{figure}

\end{apendicesenv}
