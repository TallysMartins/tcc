\chapter{Introdução}
  O uso das Tecnologias de Comunicação e Informação (TCIs)
tem sido um grande fator de transformação e influência para a democracia
\cite{levy2002ciberdemocracia, Benkler2006}.
Nos últimos anos, testemunhamos diversas manifestações políticas que se organizaram
através do uso da Internet e das TCIs, como um meio de participação social.
Neste cenário, um desafio constante é descobrir melhores maneiras de atrair e engajar cidadãos
comuns juntamente com leigos e ativistas em discussões mais inclusivas e objetivas.

  Em 2009, uma consulta pública sobre a Lei 12.965 de 2014, conhecida como Lei
do Marco Civil da
Internet\footnote{\url{https://itsrio.org/wp-content/uploads/2017/02/marco_civil_construcao_aplicacao.pdf}},
foi aberta como forma de comunicação entre governantes e população.
Nesta consulta, era possível que as pessoas fizessem comentários nos parágrafos
de um documento digital que descrevia a lei. O diálogo ocorreu em duas etapas, e foram
contabilizados aproximadamente 2.000 comentários em todo o processo.
Outras experiências de discussões online, como o debate público sobre o Código de Processo
Civil\footnote{\url{http://www.migalhas.com.br/arquivo_artigo/art20120210-05.pdf}},
mostram ainda que a maioria dos comentários é feita por uma pequena parcela dos participantes
e o nível de engajamento é relativamente baixo. A participação oscila entre algumas
dezenas ou centenas de pessoas e comentários.

  Discussões que ocorrem em vias comuns, como fóruns e ``threads'', não provêm
uma dinâmica escalável para debates com muitos participantes. O trabalho
de leitura para interpretar imensos textos de comentários espanta até mesmo os
mais interessados. Além disso, é difícil mapear nesse tipo de conversa as diferentes
opinião formadas, ou ainda quais são maioria ou minoria dentro da conversa. Uma
plataforma para discussões online deve ser capaz de prover isso aos participantes.

  Apresentamos então, o projeto Empurrando Juntos, descrito no Capítulo
\ref{chapter:empurrando-juntos}, como uma ferramenta para debates online que se baseia
em um conceito diferente para estruturação da conversa. Nesse conceito, que aqui
chamamos de participação ``crowdsource'', os participantes podem expressar sua opinião em
pequenas ações e com pouco esforço, algo comparado a funcionalidade ``Curtir'' do Facebook. Dessa maneira,
a participação se torna uma ação escalável a milhares de usuários ao empenho de
um clique. Outras experiências de consultas públicas, como
ArenaNetMundial\footnote{\url{http://www.participa.br/articles/public/0007/0286/resultado-consulta-publica-pt.pdf}},
mostraram que esta é uma ótima opção para quebrar as barreiras do
engajamento e abrir mais espaço para discussões.

  Nesse paradigma de conversa, a opinião dos participantes pode ser composta em
termos de concordo e discordo, 1 ou -1, ``\textit{like}'' e ``\textit{dislike}''. Com isso, podemos
construir uma correlação da opinião de cada usuário em cada comentário, o que
torna possível o uso de algoritmos de agrupamento de dados para obter análises
sofisticadas sobre o que as pessoas pensam sobre um assunto em uma discussão.

  Este trabalho apresenta um estudo feito sobre esses algoritmos e ferramentas
necessárias para o desenvolvimento de um modelo de clusters que identifique grupos de
opinião em uma plataforma de participação social.

  \section{Justificativa}
  Sabendo da importância da tecnologia nos dias de hoje e acreditando no seu
potencial como forma de inovação para o bem social, este trabalho oferece uma
colaboração para o projeto Empurrando Juntos na busca de uma solução que acrescente
novas possibilidades para debates democráticos.

  Complementarmente, esta pesquisa reunirá um conhecimento acerca de algoritmos de
clusterização de forma que possa servir de insumo para outros estudos relacionados.

  \section{Objetivos}
  \subsection{Objetivo Geral}
  \label{objetivo-geral}

  O objetivo principal desse trabalho é propor um algoritmo que identifique grupos
de opinião em uma discussão online, com base nas reações (concordo, discordo, pular) de usuários aos comentários da
discussão feito por outros usuários.

  Numa primeira etapa foi feito um estudo teórico sobre estratégias de clusterização,
para adquirir um domínio sobre os diferentes modelos e algoritmos. A partir deste
estudo, desenvolvemos soluções baseadas nos algoritmos selecionados.

  Executada a parte prática de implementação, será feita uma avaliação
dos resultados comparando-os com resultados esperados, obtidos a partir de um
modelo estatístico e um índice de validação para algoritmos de clusterização.

  \subsection{Objetivos Específicos}
  \label{objetivos-especificos}
  Buscando alcançar e satisfazer o objetivo geral, foram definidos seis objetivos específicos, os quais são descritos a seguir:

  \begin{itemize}
    \item Definir critérios da solução
    \item Efetuar estudo técnico sobre clusterização de dados
    \item Selecionar técnicas de clusterização a serem aplicadas
    \item Definir tecnologias e arquitetura para a solução
    \item Construir solução com base nos critérios definidos
    \item Analisar resultados da solução
  \end{itemize}

  \section{Organização do Trabalho}

  O desenvolvimento do trabalho é dividido em 6 Capítulos. Primeiramente,
  no Capítulo \ref{chapter:2}, apresentamos uma revisão bibliográfica a respeito de
  algoritmos de clusterização. Logo após, nos Capítulos \ref{chapter:4} e \ref{chapter:5},
  é dada uma contextualização do problema junto ao contexto da plataforma Empurrando
  Juntos e uma proposta de solução é apresentada. Por fim, nos Capítulos \ref{chapter:6} e
 \ref{chapter:7}, são apresentados respectivamente
  os resultados atingidos e conclusões sobre estes resultados, bem como suas
  limitações, pesquisas futuras e outras considerações finais.
