\begin{apendicesenv}

\partapendices

\chapter{Técnicas}
\label{apendice:técnicas}

\section{Análise de Componentes Principais - PCA}
\label{apendice:pca}

  Esta é uma técnica é comumente utilizada para transformação dos dados com o objetivo de
  reduzir sua dimensão, quando há um grande número de variáveis nos dados, o que
  pode causar o problema da maldição da dimensionalidade.
  Elucidamos a seguir os passos mostrados na tabela \ref{fig:pca-steps} para
  aplicação do PCA para reduzir um conjunto de dados de duas para uma dimensão.
  
  O gráfico da figura \ref{fig:pca-dataset} abaixo mostra os valores relacionados
  para as variáveis X1 e X2.

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.6\linewidth]{figuras/pca-dataset.eps}
    \label{fig:pca-dataset}
  \end{figure}

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/plot-points.eps}
    \caption{Pontos plotados. Fonte: NeuralDesigner}
    \label{fig:pca-dataset}
  \end{figure}

  Então, como primeiro passo para a análise dos componentes principais,
  centralizamos os dados em torno dos eixos subtraindo a média de cada
  variável no conjunto de pontos, como mostra a figura \ref{fig:pca-dataset-centered}.

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/plot-centered-points.eps}
    \caption{Pontos centralizados. Fonte: NeuralDesigner}
    \label{fig:pca-dataset-centered}
  \end{figure}

  Após a translação do eixo, calculamos a matriz de covariância entre as variáveis,
  o quenos dá informações sobre a relação que elas possuem. Se a covariância é positiva,
  então as duas variáveis crescem e decrescem juntas. Se seu valor é negativo,
  então enquanto uma variável cresce, a outra decresce e vice-versa. Esses
  valores determinam a dependência linear entre as variáveis que serão
  usadas para redução da dimensão dos dados, como mostra a tabela
  \ref{tab:covariance-matrix}.

  \begin{table}[!htbp]
    \begin{center}
      \begin{tabular}{|p{2cm}|p{2cm}|p{2cm}|} \hline
        -  & \textbf{X1} & \textbf{X2} \\\hline\hline
        \textbf{X1} & 0.33 & 0.25 \\\hline
        \textbf{X2} & 0.25 & 0.41 \\\hline
      \end{tabular}
      \caption{\label{tab:covariance-matrix}Matriz de covariância entre as variáveis}
    \end{center}
  \end{table}

  Os valores da diagonal principal representam a covariância de cada variável
  com ela mesma, o que é igual a sua medida de variância. A variância mede o quão dispersos
  os dados estão em relação a média. A diagonal secundária mostra a covariância entre
  as duas variáveis. Os valores positivos mostram que as duas crescem e decrescem
  juntas.

  Podemos então encontrar os autovetores e autovalores da matriz de covariância.
  Um autovetor é definido como aquele vetor que mantém sua direção
  quando qualquer transformação linear é aplicada sobre ele. No entanto, seu módulo
  pode não permanecer o mesmo após a transformação, por exemplo quando multiplicamos
  este por uma escalar. Esta escalar é então chamada de autovalor e cada autovetor
  possui um autovalor associado.

  O número de autovetores ou componentes que podemos calcular um conjunto de dados
  é igual à dimensão deste conjunto. Neste exemplo, com um conjunto de duas dimensões
  representados pelas variáveis X1 e X2, o número de autovetores será igual a 2.
  A figura \ref{fig:eigenvectors} representa os autovetores do exemplo:

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.9\linewidth]{figuras/data-eigenvectors.eps}
    \caption{Autovetores dos dados. Fonte: NeuralDesigner adaptado}
    \label{fig:eigenvectors}
  \end{figure}

  Os autovetores calculados a partir da matriz de covariância representam as direções
  em que os dados acumulam a maior variância. Já os respectivos autovelores determinam
  a quantidade de variância naquela direção. Uma vez que obtemos os autovetores,
  podemos rotacionar os dados para as novas coordenadas, como pode ser visto na
  figura \ref{fig:eigenvectors-projection}.

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.9\linewidth]{figuras/eigenvectors-rotation.eps}
    \caption{Representação em termo dos autovetores. Fonte: NeuralDesigner adaptado}
    \label{fig:eigenvectors-projection}
  \end{figure}

  Então, dentre os autovetores calculados, é preciso selecionar aqueles onde
  serão projetados os dados. Os autovetores selecionados são chamados componentes
  principais.

  O critério para seleção dos autovetores é o total de variância que ele representa.
  Como já foi dito, essa variância é representada pelo autovalor de cada autovetor.
  A soma dos autovalores é igual a variância total dos dados. Se queremos projetar
  os dados para 1 dimensão, selecionamos o autovetor que possui o maior autovalor.

  A tabela \ref{tab:eigenvalues} mostra os autovalores para os autovetores
  calculados. Podemos observar que o primeiro autovetor representa cerca de 85\%
  da variância dos dados, enquanto o segundo explica cerca de 15\%. Os valores
  acumulados somam a variância total.

  \begin{table}[!htbp]
    \begin{center}
      \begin{tabular}{|p{2cm}|p{5cm}|p{5cm}|} \hline
        -  & \textbf{Autovalores/Variância} & \textbf{Variância acumulada} \\\hline\hline
        \textbf{1} & 84.5979 & 84.5979 \\\hline
        \textbf{2} & 15.4021 & 100 \\\hline
      \end{tabular}
      \caption{Autovalores dos autovetores}
      \label{tab:eigenvalues}
    \end{center}
  \end{table}

  Uma maneira comum de selecionar os autovetores é estabelecer um valor limite
  de variância desejada para o resultado. Por exemplo, se queremos manter 90\%
  da representatividade, selecionamos quantos componentes quanto necessários para
  obter este valor, que é o acumulado da soma das variâncias de cada componente.
  Como neste exemplo iremos aplicar a redução para 1 dimensão, será selecionado
  o primeiro autovetor como componente principal. Por consequente, o resultado final
  terá 85\% da representatividade dos dados originais.

  Selecionados os componentes, o último passo é projetar os dados nas novas
  coordenadas. A figura \ref{fig:eigenvectors-result-projection} mostra o resultado
  desta projeção.

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=.9\linewidth]{figuras/eigenvector-result-projection.eps}
    \caption{Projeção dos dados no componente selecionado. Fonte: NeuralDesigner adaptado}
    \label{fig:eigenvectors-result-projection}
  \end{figure}


\section{Decomposição do Valor Singular}
\label{apendice:svd}


% \chapter{Segundo Apêndice}

% Texto do segundo apêndice.

\end{apendicesenv}
